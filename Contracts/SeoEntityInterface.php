<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Contracts;

interface SeoEntityInterface
{
    public function getTitle(): ?string;

    public function getContent(): ?string;
}
