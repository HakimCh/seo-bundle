<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Contracts;

interface GeneratorInterface
{
    /**
     * @param array $options
     * @param array $tags
     *
     * @return array
     */
    public function process(array $options, $tags = []): array;
}
