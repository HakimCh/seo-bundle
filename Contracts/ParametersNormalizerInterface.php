<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Contracts;

interface ParametersNormalizerInterface
{
    public function guess(array $parameters): array;

    public function format(array $parameters, string $value): string;

    public function normalize(array $parameters): array;
}
