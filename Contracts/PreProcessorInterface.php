<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Contracts;

interface PreProcessorInterface
{
    /**
     * @param array $elements
     *
     * @return mixed
     */
    public function process(array $elements);
}
