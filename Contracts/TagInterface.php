<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Contracts;

interface TagInterface
{
    /**
     * @param mixed $key
     * @param mixed $value
     *
     * @return TagInterface
     */
    public function addOption($key, $value);

    /**
     * @return TagInterface
     */
    public function end();
}
