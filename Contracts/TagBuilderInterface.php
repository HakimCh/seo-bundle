<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Contracts;

interface TagBuilderInterface
{
    /**
     * Instantiate the TagType.
     *
     * @param string $tag
     *
     * @return TagInterface
     */
    public function addType(string $tag);

    /**
     * Add Tag.
     *
     * @param TagInterface $tag
     *
     * @return mixed
     */
    public function addTag(TagInterface $tag);
}
