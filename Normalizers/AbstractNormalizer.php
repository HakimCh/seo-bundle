<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Normalizers;

/**
 * Class AbstractNormalizer.
 */
abstract class AbstractNormalizer
{
    /**
     * @var array
     */
    protected $formats = [];

    /**
     * @var array
     */
    protected $parameters = [];

    public function __construct(array $parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * @param array  $parameters
     * @param string $value
     *
     * @return string
     */
    abstract public function format(array $parameters, string $value): string;

    /**
     * @param array $parameters
     *
     * @return array
     */
    abstract public function normalize(array $parameters): array;

    /**
     * @param array $parameter
     *
     * @return array
     */
    public function guess(array $parameter): array
    {
        $parameters = $this->normalize(array_replace(
            $this->parameters,
            $parameter
        ));

        return [
            $this->format($parameters, 'title'),
            $this->format($parameters, 'description'),
            $parameters,
        ];
    }

    /**
     * @param string $format
     * @param array  $arguments
     * @param string $pattern
     *
     * @return string
     */
    protected function printFormat(string $format, array $arguments, string $pattern = "/\{{(\w+)\}}/"): string
    {
        return preg_replace_callback($pattern, function ($matches) use ($arguments) {
            return @$arguments[$matches[1]] ?: $matches[0];
        }, $format);
    }
}
