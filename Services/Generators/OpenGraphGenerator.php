<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Generators;

use HakimCh\SeoBundle\Contracts\GeneratorInterface;

class OpenGraphGenerator extends AbstractGenerator implements GeneratorInterface
{
    protected $elementFormat = '{"meta": {"property": "og:%s", "content": "%s"}}';
}
