<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Generators;

use HakimCh\SeoBundle\Contracts\GeneratorInterface;

class LDJsonGenerator extends AbstractGenerator implements GeneratorInterface
{
    /**
     * @var string
     */
    protected $elementFormat = '{"script": {"type": ""}}';

    /**
     * @param array $options
     * @param array $tags
     *
     * @return array
     */
    public function process(array $options, $tags = []): array
    {
        $html = '<script type="application/ld+json">';
        $html .= json_encode($options);
        $html .= '</script>';

        return [$html];
    }
}
