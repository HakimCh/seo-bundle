<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Generators;

use HakimCh\SeoBundle\Contracts\GeneratorInterface;

class DefaultGenerator extends AbstractGenerator implements GeneratorInterface
{
    protected $elementFormat = [
        'title' => '{"%s": "%s"}',
        'description' => '{"meta": {"name": "%s", "content": "%s"}}',
        'canonical' => '{"link": {"rel": "%s", "href": "%s"}}',
    ];

    /**
     * @param array $options
     * @param array $tags
     *
     * @return array
     */
    public function process(array $options, $tags = []): array
    {
        foreach ($options as $key => $value) {
            if (isset($this->elementFormat[$key])) {
                $tags[] = $this->render($key, $value, $this->elementFormat[$key]);
            }
        }

        $tagsJson = sprintf('[%s]', implode(',', $tags));

        return json_decode($tagsJson, true);
    }
}
