<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Generators;

use HakimCh\SeoBundle\Contracts\GeneratorInterface;

class SchemaGenerator extends AbstractGenerator implements GeneratorInterface
{
    /**
     * @var string
     */
    protected $elementFormat = '{"meta": {"itemprop": "%s", "content": "%s"}}';
}
