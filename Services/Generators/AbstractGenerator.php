<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Generators;

use HakimCh\SeoBundle\Contracts\PreProcessorInterface;

class AbstractGenerator
{
    /**
     * @var string
     */
    protected $elementFormat;
    /**
     * @var PreProcessorInterface
     */
    protected $preProcessor;

    public function __construct(PreProcessorInterface $preProcessor)
    {
        $this->preProcessor = $preProcessor;
    }

    /**
     * @param array $options
     * @param array $tags
     *
     * @return array
     */
    public function process(array $options, $tags = []): array
    {
        foreach ($options as $key => $value) {
            if (\is_array($value)) {
                foreach ($value as $k => $v) {
                    $tags[] = $this->render($key.':'.$k, $v);
                }
                continue;
            }
            $tags[] = $this->render($key, $value);
        }

        $tagsJson = sprintf('[%s]', implode(',', $tags));

        return json_decode($tagsJson, true);
    }

    /**
     * @param $key
     * @param $value
     * @param null $format
     *
     * @return string
     */
    protected function render($key, $value, $format = null)
    {
        if (null === $format) {
            $format = $this->elementFormat;
        }

        return vsprintf($format, [$key, $value]);
    }
}
