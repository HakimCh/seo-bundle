<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Generators;

use HakimCh\SeoBundle\Contracts\GeneratorInterface;

class TwitterGenerator extends AbstractGenerator implements GeneratorInterface
{
    //protected $elementFormat = '<meta name="twitter:%s" content="%s" />';
    protected $elementFormat = '{"meta": {"name": "twitter:%s", "content": "%s"}}';
}
