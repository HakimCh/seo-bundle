<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services;

use HakimCh\SeoBundle\Contracts\PreProcessorInterface;
use HakimCh\SeoBundle\Exceptions\SeoTagTypeNotFoundException;
use HakimCh\SeoBundle\Services\Types\OpenGraph;
use HakimCh\SeoBundle\Services\Types\Schema;
use HakimCh\SeoBundle\Services\Types\Twitter;
use Symfony\Component\HttpFoundation\RequestStack;

class SeoFactory
{
    /**
     * @var array
     */
    private $options;
    /**
     * @var TagBuilder
     */
    private $tagBuilder;
    /**
     * @var PreProcessorInterface
     */
    private $preProcessor;

    public function __construct(RequestStack $requestStack, TagBuilder $tagBuilder, PreProcessorInterface $preProcessor, array $options)
    {
        if ($requestStack->getCurrentRequest()) {
            $options['url'] = $requestStack->getCurrentRequest()->getUri();
        }
        $this->preProcessor = $preProcessor;
        $this->options = $options;
        $this->tagBuilder = $tagBuilder;
    }

    /**
     * @param string $title
     * @param string $description
     * @param array  $extraOptions
     *
     * @throws SeoTagTypeNotFoundException
     *
     * @return mixed
     */
    public function create(string $title, string $description, array $extraOptions = [])
    {
        $this->options = array_merge($this->options, $extraOptions);

        $this->options['title'] = $title;
        $this->options['description'] = $description;
        $this->tagBuilder->createMetaTags($this->options, $this->preProcessor)
            ->addType(Schema::class)->end()
            ->addType(Twitter::class)->end()
            ->addType(OpenGraph::class)->end();

        return $this->tagBuilder->generate();
    }
}
