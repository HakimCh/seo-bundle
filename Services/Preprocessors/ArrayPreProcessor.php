<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Preprocessors;

use HakimCh\SeoBundle\Contracts\PreProcessorInterface;

class ArrayPreProcessor implements PreProcessorInterface
{
    /**
     * @param array $elements
     *
     * @return array|mixed
     */
    public function process(array $elements)
    {
        return $elements;
    }
}
