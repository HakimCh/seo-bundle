<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Preprocessors;

use HakimCh\SeoBundle\Contracts\PreProcessorInterface;

class HtmlPreProcessor implements PreProcessorInterface
{
    /**
     * @param array $elements
     *
     * @return mixed|string
     */
    public function process(array $elements)
    {
        $html = '';
        foreach ($elements as $element) {
            if (\is_array($element)) {
                foreach ($element as $name => $content) {
                    if (\is_array($content)) {
                        $html .= $this->renderElement($name, $content);
                        continue;
                    }
                    $html .= $this->renderBloc($name, $content);
                }
                continue;
            }
            $html .= $element;
        }

        return $html;
    }

    /**
     * @param $name
     * @param $content
     *
     * @return string
     */
    public function renderBloc($name, $content)
    {
        return '<'.$name.'>'.$content.'</'.$name.'>'.PHP_EOL;
    }

    /**
     * @param $name
     * @param $value
     *
     * @return string
     */
    public function renderElement($name, $value)
    {
        $attributes = [];
        foreach ($value as $key => $value) {
            $attributes[] = sprintf('%s="%s"', $key, $value);
        }

        return '<'.$name.' '.implode(' ', $attributes).'/>'.PHP_EOL;
    }
}
