<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Types;

use HakimCh\SeoBundle\Services\Generators\SchemaGenerator;

class Schema extends AbstractTagType
{
    /**
     * @var string
     */
    protected $generatorName = SchemaGenerator::class;
    /**
     * @var array
     */
    protected $allowedKeys = ['name', 'description', 'image'];

    /**
     * @param array $options
     *
     * @return array
     */
    public function normalize(array $options): array
    {
        if (\array_key_exists('title', $options)) {
            $options['name'] = $options['title'];
        }

        return parent::normalize($options);
    }
}
