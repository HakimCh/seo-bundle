<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Types;

use HakimCh\SeoBundle\Services\Generators\OpenGraphGenerator;

class OpenGraph extends AbstractTagType
{
    /**
     * @var string
     */
    protected $generatorName = OpenGraphGenerator::class;
    /**
     * @var array
     */
    protected $allowedKeys = ['title', 'type', 'url', 'image', 'description', 'site_name', 'admins', 'price', 'locale'];

    /**
     * @param array $options
     *
     * @return array
     */
    public function normalize(array $options): array
    {
        if (!\array_key_exists('type', $options)) {
            $options['type'] = 'article';
        }

        return parent::normalize($options);
    }
}
