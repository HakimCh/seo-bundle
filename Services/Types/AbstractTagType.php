<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Types;

use HakimCh\SeoBundle\Contracts\GeneratorInterface;
use HakimCh\SeoBundle\Contracts\PreProcessorInterface;
use HakimCh\SeoBundle\Contracts\TagBuilderInterface;
use HakimCh\SeoBundle\Contracts\TagInterface;
use HakimCh\SeoBundle\Exceptions\SeoTagTypeNotLoadedException;
use HakimCh\SeoBundle\Services\Generators\DefaultGenerator;

class AbstractTagType implements TagInterface
{
    /**
     * @var array
     */
    protected $options;
    /**
     * @var TagInterface
     */
    protected $tagObject;
    /**
     * @var TagBuilderInterface
     */
    protected $parent;
    /**
     * @var string
     */
    protected $generatorName = DefaultGenerator::class;
    /**
     * @var GeneratorInterface
     */
    protected $generator;
    /**
     * @var array
     */
    protected $allowedKeys = ['title', 'description', 'canonical'];

    public function __construct(TagBuilderInterface $parent, array $options, PreProcessorInterface $preProcessor)
    {
        $this->parent = $parent;
        $this->tagObject = $this;
        $this->options = $this->normalize($options);
        $this->generator = new $this->generatorName($preProcessor);
    }

    public function normalize(array $options): array
    {
        return array_intersect_key($options, array_flip($this->allowedKeys));
    }

    /**
     * @param mixed $key
     * @param mixed $value
     *
     * @throws SeoTagTypeNotLoadedException
     *
     * @return TagInterface
     */
    public function addOption($key, $value)
    {
        if (!$this->tagObject instanceof TagInterface) {
            throw new SeoTagTypeNotLoadedException();
        }
        $this->options[$key] = $value;

        return $this->tagObject;
    }

    /**
     * @return TagBuilderInterface
     */
    public function end()
    {
        $this->parent->addTag($this);

        return $this->parent;
    }

    /**
     * @return array
     */
    public function generate()
    {
        return $this->generator->process($this->options);
    }
}
