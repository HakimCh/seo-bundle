<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Types;

use HakimCh\SeoBundle\Services\Generators\TwitterGenerator;

class Twitter extends AbstractTagType
{
    /**
     * @var string
     */
    protected $generatorName = TwitterGenerator::class;
    /**
     * @var array
     */
    protected $allowedKeys = ['card', 'site', 'title', 'description', 'creator', 'image'];

    /**
     * @param array $options
     *
     * @return array
     */
    public function normalize(array $options): array
    {
        if (!\array_key_exists('card', $options)) {
            $options['card'] = 'summary';
        }

        return parent::normalize($options);
    }
}
