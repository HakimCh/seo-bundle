<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services\Types;

use HakimCh\SeoBundle\Services\Generators\LDJsonGenerator;

class LDJson extends AbstractTagType
{
    /**
     * @var string
     */
    protected $generatorName = LDJsonGenerator::class;
    /**
     * @var array
     */
    protected $allowedKeys = ['@context', '@type', 'logo', 'url', 'description', 'address', 'telephone', 'email'];

    /**
     * @param array $options
     *
     * @return array
     */
    public function normalize(array $options): array
    {
        if (\array_key_exists('context', $options)) {
            $options['@context'] = $options['context'];
        } else {
            $options['@context'] = 'http://schema.org';
        }
        if (\array_key_exists('type', $options)) {
            $options['@type'] = $options['type'];
        } else {
            $options['@type'] = 'Organization';
        }
        if (\array_key_exists('image', $options)) {
            $options['logo'] = $options['image'];
        }
        if (\array_key_exists('address', $options) && \is_array($options['address'])) {
            $options['logo'] = json_encode($options['address']);
        }

        return parent::normalize($options);
    }
}
