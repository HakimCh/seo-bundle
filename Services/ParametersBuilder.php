<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services;

use HakimCh\SeoBundle\Contracts\SeoEntityInterface;
use HakimCh\SeoBundle\Exceptions\ParameterNormalizerNotFoundException;
use HakimCh\SeoBundle\Normalizers\AbstractNormalizer;

class ParametersBuilder
{
    /**
     * @var array
     */
    private $data = [];
    /**
     * @var AbstractNormalizer[]
     */
    private $normalizer;

    public function __construct(?string $normalizer = null, array $parameters = [])
    {
        if ($normalizer) {
            $this->normalizer = new $normalizer($parameters);
        }
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function addTitle(string $title): self
    {
        $this->data['seo']['title'] = ucfirst(mb_strtolower($title));

        return $this;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function addDescription(string $description): self
    {
        $description = strip_tags($description);
        $description = preg_replace('/[^A-Za-z0-9]/', ' ', $description);
        $description = preg_replace("/\s\s+/", ' ', $description);
        $description = ucfirst(mb_strtolower($description));
        $this->data['seo']['description'] = substr(trim($description), 0, 170);

        return $this;
    }

    /**
     * @param array $parameters
     *
     * @return self
     */
    public function buildFromGlobal(array $parameters): self
    {
        if (!$this->normalizer instanceof AbstractNormalizer) {
            throw new ParameterNormalizerNotFoundException('No normalizer found for formating the global parameter');
        }
        $this->addDatas($parameters);
        [$title, $description, $data] = $this->normalizer->guess($this->data);
        $this->addTitle($title);
        $this->addDescription($description);
        $this->addDatas($data);

        return $this;
    }

    /**
     * @param SeoEntityInterface $item
     *
     * @return self
     */
    public function buildFromEntity(SeoEntityInterface $item): self
    {
        $this->addTitle($item->getTitle());
        $this->addDescription($item->getContent());
        if (method_exists($item, 'getPictures')) {
            $this->data['seo']['options']['image'] = current($item->getPictures());
        }
        $this->addData('item', $item);

        return $this;
    }

    /**
     * @param array $data
     *
     * @return Parameters
     */
    public function addDatas(array $data): self
    {
        $this->data = array_replace($this->data, $data);

        return $this;
    }

    /**
     * @param mixed $key
     * @param mixed $value
     *
     * @return self
     */
    public function addData($key, $value): self
    {
        $this->data[$key] = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }
}
