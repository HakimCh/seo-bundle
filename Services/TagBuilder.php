<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Services;

use HakimCh\SeoBundle\Contracts\PreProcessorInterface;
use HakimCh\SeoBundle\Contracts\TagBuilderInterface;
use HakimCh\SeoBundle\Contracts\TagInterface;
use HakimCh\SeoBundle\Exceptions\SeoTagTypeNotFoundException;
use HakimCh\SeoBundle\Services\Types\AbstractTagType;

class TagBuilder implements TagBuilderInterface
{
    /**
     * @var array
     */
    private $options;
    /**
     * @var TagInterface[]
     */
    private $tags = [];
    /**
     * @var PreProcessorInterface
     */
    private $preProcessor;

    /**
     * @param array                 $options
     * @param PreProcessorInterface $preProcessor
     *
     * @throws SeoTagTypeNotFoundException
     *
     * @return self
     */
    public function createMetaTags(array $options, PreProcessorInterface $preProcessor): self
    {
        $this->options = $options;
        $this->preProcessor = $preProcessor;
        $this->addType(AbstractTagType::class)->end();

        return $this;
    }

    /**
     * Instantiate the TagType.
     *
     * @param string $tag
     *
     * @throws SeoTagTypeNotFoundException
     *
     * @return TagInterface
     */
    public function addType(string $tag): TagInterface
    {
        if (!class_exists($tag)) {
            throw new SeoTagTypeNotFoundException($tag);
        }

        return new $tag($this, $this->options, $this->preProcessor);
    }

    /**
     * @param TagInterface $tag
     *
     * @return mixed|void
     */
    public function addTag(TagInterface $tag): void
    {
        $this->tags[] = $tag;
    }

    public function generate(array $tags = [])
    {
        foreach ($this->tags as $tag) {
            $tags = array_merge($tags, $tag->generate());
        }

        return $this->preProcessor->process($tags);
    }
}
