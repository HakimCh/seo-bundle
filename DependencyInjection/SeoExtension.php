<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class SeoExtension implements ExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yaml');
        $definition = $container->getDefinition('HakimCh\SeoBundle\Services\SeoFactory');
        $definition->setArguments([
            '$options' => $configs[0]['options'],
            '$preProcessor' => $container->getDefinition('HakimCh\SeoBundle\Services\Preprocessors\HtmlPreProcessor'),
        ]);
    }

    public function getAlias()
    {
        return 'seo';
    }

    public function getXsdValidationBasePath()
    {
        // TODO: Implement getXsdValidationBasePath() method.
    }

    public function getNamespace()
    {
        // TODO: Implement getNamespace() method.
    }
}
