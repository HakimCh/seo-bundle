<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('seo');

        $treeBuilder
            ->getRootNode()
            ->children()
                ->arrayNode('options')->defaultValue([])->end()
            ->end();

        return $treeBuilder;
    }
}
