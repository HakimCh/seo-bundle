<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Twig;

use HakimCh\SeoBundle\Exceptions\SeoTagTypeNotFoundException;
use HakimCh\SeoBundle\Services\SeoFactory;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SeoExtension extends AbstractExtension
{
    /**
     * @var SeoFactory
     */
    private $seoFactory;

    public function __construct(SeoFactory $seoFactory)
    {
        $this->seoFactory = $seoFactory;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('seo', [$this, 'seoFunction'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param $title
     * @param $description
     * @param array|null $options
     *
     * @throws SeoTagTypeNotFoundException
     *
     * @return mixed
     */
    public function seoFunction($title, $description, ?array $options = [])
    {
        return $this->seoFactory->create($title.' | Sportautomoto.ma', $description, $options);
    }
}
