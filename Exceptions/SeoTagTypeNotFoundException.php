<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Exceptions;

use Throwable;

class SeoTagTypeNotFoundException extends \Exception
{
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('The Tag Type %s not found', $message), $code, $previous);
    }
}
