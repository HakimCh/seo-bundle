<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Exceptions;

use Throwable;

class SeoTagTypeNotLoadedException extends \Exception
{
    public function __construct(string $message = 'The Tag Type is not loaded', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
