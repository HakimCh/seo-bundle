<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Exceptions;

use Throwable;

class SeoGeneratorNotFoundException extends \Exception
{
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('The %s not found', $message), $code, $previous);
    }
}
