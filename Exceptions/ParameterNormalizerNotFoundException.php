<?php

declare(strict_types=1);

namespace HakimCh\SeoBundle\Exceptions;

class ParameterNormalizerNotFoundException extends \Exception
{
}
